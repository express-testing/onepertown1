/**
 * Site Configuration
 * ==================
 * You can use this configuration file everywhere
 * in your templates by using the config variable.
 * Example: <%= config.maps.google.api_key %>
 */
const js_core_version = '1.3.5'; // version of the current js framework
const timestamp = (new Date()).getTime();

module.exports = {
 "sanbox_version": 1.1,
 "timestamp": 1595524909510,
 "name": "Headless Flash",
 "url": "https://www.togetherdigital.ie",
 "tab_color": "#1779BA",
 "storyblok": {
  "space_id": 89453,
  "api_token": "JVRRymrsQ7Xpee26db5XxAtt",
  "folder": "storyblok",
  "layout": "page",
  "datasources": []
 },
 "replace": [],
 "modules_without_detail": [],
 "modules_with_optional_detail_page": [],
 "modules_with_optional_detail_page_without_fallback": [],
 "sitemap": {
  "include_modules_common": [
   "Page",
   "Folder",
   "Standard Page Module"
  ],
  "include_modules_xml": [],
  "include_modules_page": [],
  "exclude_pages_common": [
   "settings",
   "sitemap",
   "homepage",
   "almost-finished",
   "thank-you",
   "search"
  ],
  "exclude_pages_xml": [
   "privacy"
  ],
  "exclude_pages_page": [],
  "exclude_folders": [
   "relationship"
  ]
 },
 "criticalcss": {
  "include": []
 },
 "maps": {
  "google": {
   "api_key": "AIzaSyDCxmXh8p-WQYfiULY3a6nAK-AqWj_7_BI"
  }
 },
 "recaptcha": "6LcdnogUAAAAAJEGFie7rWPfAScZmcbJe57Uawhb",
 "google_tag_manager": "",
 "woopra": "",
 "fonts": {
  "google": [
   {
    "name": "Source Sans Pro",
    "sizes": "400,300,600"
   }
  ],
  "typekit_id": "",
  "custom": []
 },
 "imgix": {
  "source": "togetherdigital.imgix.net",
  "secure_url_token": "XCfRGqWFra7knft7",
  "to_remove_from_url": "//a.storyblok.com/f/"
 },
 "build": {
  "folder": "build",
  "include": [
   "assets",
   "robots.txt"
  ],
  "exclude": [
   "assets/scss",
   "assets/js/plugins",
   "assets/js/polyfills",
   "assets/js/custom",
   "assets/js/flash"
  ]
 },
 "watch": {
  "build": [
   "config.js",
   "data/**/*.json",
   "layouts/**/*.html",
   "pages/**/*.html",
   "snippets/**/*.html",
   "blocks/**/*.html"
  ],
  "assets": [
   "assets/scss/**/*.scss",
   "assets/js/plugins/*.js",
   "assets/js/custom/*.js",
   "assets/js/flash/1.3.5/core/**/*.js",
   "assets/js/flash/1.3.5/libraries/*.js",
   "blocks/**/style.scss",
   "blocks/**/script.js"
  ]
 },
 "assets_compile": {
  "assets/css/icons.min.css": [
   "assets/fonts/flaticon/_flaticon.scss"
  ],
  "assets/css/bundle.min.css": [
   "assets/scss/main.scss",
   "blocks/**/style.scss"
  ],
  "assets/js/bundle.min.js": [
   "assets/js/polyfills/*.js",
   "assets/js/flash/1.3.5/core/**/*.js",
   "assets/js/flash/1.3.5/libraries/*.js",
   "assets/js/flash/1.3.5/plugins/*.js",
   "assets/js/plugins/*.js",
   "assets/js/custom/*.js",
   "blocks/**/script.js",
   "assets/js/flash/1.3.5/start.js"
  ]
 }
};